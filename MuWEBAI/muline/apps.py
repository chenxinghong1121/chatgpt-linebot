from django.apps import AppConfig


class MulineConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'muline'
